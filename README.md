# WxWidgets
This is a package for WxWidgets.
It just provides a build.sh script that has to be run on your machine if no release builds are available.

Usage with cmake:
``` cmake
EXEC_PROGRAM(uname ARGS -m OUTPUT_VARIABLE UNAME_M)
EXEC_PROGRAM(uname ARGS -s OUTPUT_VARIABLE UNAME_S)

# Include wxWidgets
set(wxWidgets_ROOT_DIR ../gp_wxwidgets/build/${UNAME_M}_${UNAME_S}/)
set(wxWidgets_USE_STATIC ON)
find_package(wxWidgets COMPONENTS gl core base OPTIONAL_COMPONENTS net)
include(${wxWidgets_USE_FILE})

...

target_link_libraries(main PRIVATE ${wxWidgets_LIBRARIES})
```

Include wxWidgets in a precompiled header.
Include it as static library.